package uk.ac.warwick.cs126.structures;

import java.util.Comparator;

public class QuickSorter<E> {
    public E[] quickSort(E[] arrToSort, int start, int end, Comparator<E> c) {
        if (arrToSort == null || arrToSort.length == 0) {return arrToSort;}
        int pivot = pivot(arrToSort, start, end, c);

        if (pivot - 1 > start) {
            quickSort(arrToSort, start, pivot - 1, c);
        }

        if (pivot + 1 < end) {
            quickSort(arrToSort, pivot + 1, end, c); 
        }

        return arrToSort;
    }

    public int pivot(E[] arrToSort, int start, int end, Comparator<E> c) {
        E pivotObject = arrToSort[end];

        for (int i = start; i < end; i++) {
            if (c.compare(arrToSort[i], pivotObject) < 0) {
                E tempElement = arrToSort[start];
                arrToSort[start] = arrToSort[i];
                arrToSort[i] = tempElement; 
                start += 1; 
            }
        }
        
        E tempElement = arrToSort[start]; 
        arrToSort[start] = pivotObject; 
        arrToSort[end] = tempElement; 

        return start; 
    }
    
}

