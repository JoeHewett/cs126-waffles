package uk.ac.warwick.cs126.structures;

/**
 * Code is based on code from Lab4 but has been modified to fulfil the necessary criteria for the Stores.
*/
import javax.lang.model.element.Element;

public class MyListElement<E> {

    private final E value;
    private MyListElement<E> nextElement;
    private MyListElement<E> prevElement;
    
    public MyListElement(E value) {
        this.value = value;
    }
    
    public E getValue() {
        return this.value;
    }
    
    public MyListElement<E> getNext() {
        return this.nextElement;
    }
    
    public MyListElement<E> getPrev() {
        return this.prevElement;
    }
    
    public void setNext(MyListElement<E> element) {
        this.nextElement = element;
    }
    
    public void setPrev(MyListElement<E> element) {
        this.prevElement = element;
    }

}
