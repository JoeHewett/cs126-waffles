package uk.ac.warwick.cs126.structures;

/**
 * Code is based on code from Lab4 but has been modified to fulfil the necessary criteria for the Stores.
*/
@SuppressWarnings("unchecked") 
public class MyHashSet<K extends Comparable<K>> {

    private MyHashMap<K, K> hs;

    public MyHashSet() {
        hs = new MyHashMap<>(); 
    }
    
    public void remove(K key) {
        hs.remove(key); 
    }

    public void add(K key) {
       hs.put(key, key); 
    }

    public boolean contains(K key) {
        if (hs.get(key) == null) {
            return false; 
        } else {
            return true; 
        }
    }
}
