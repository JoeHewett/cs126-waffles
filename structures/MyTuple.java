package uk.ac.warwick.cs126.structures;

public class MyTuple<L extends Comparable<L>,R> implements Comparable<MyTuple<L,R>> {
    private L leftVal;
    private R rightVal;

    public MyTuple(L left, R right) {
        leftVal = left;
        rightVal = right;

    }

    public int compareTo(MyTuple<L, R> o) {
        return leftVal.compareTo(o.getLeft()); 
    }

    public L getLeft() {
        return leftVal;
    }

    public R getRight() {
        return rightVal; 
    }
    
    public int hashCode() { 
        return leftVal.hashCode() ^ rightVal.hashCode(); 
    }
    
    @SuppressWarnings("unchecked")
    public boolean equals(Object o) {
        if (!(o instanceof MyTuple)) {
            return false;
        }
        MyTuple<L,R> tuple = (MyTuple<L,R>) o;
        return (leftVal.equals(tuple.getLeft())
                && rightVal.equals(tuple.getRight()));
    }

    
}