package uk.ac.warwick.cs126.structures;


public class MyKeyValuePair<K extends Comparable<K>,V> {

    protected K key;
    protected V value;
    
    public MyKeyValuePair(K k, V v) {
        key = k;
        value = v;
    }
    
    public K getKey() {
        return key;
    }
    
    public V getValue() {
        return value;
    }

    public void setValue(V newVal) {
        value = newVal; 
    }

    // public int compareTo(MyKeyValuePair<K,V> o) {
    //     return o.getKey().compareTo(this.getKey());
    // }
}
