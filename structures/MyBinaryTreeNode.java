package uk.ac.warwick.cs126.structures;

/**
 * Code is based on code from Lab4 but has been modified to fulfil the necessary criteria for the Stores.
*/
public class MyBinaryTreeNode<E> {
    
    private E value; 
    private MyBinaryTreeNode<E> left; 
    private MyBinaryTreeNode<E> right;
    private boolean removed;

    public MyBinaryTreeNode(E newVal) {
        value = newVal; 
        left = null; 
        right = null; 
        removed = false; 
    }

    public void setRemoved() {
        this.removed = true; 
    }
    
    public boolean isRemoved() {
        return removed;
    }

    public E getValue() {
        return value; 
    }

    public MyBinaryTreeNode<E> getLeft() {
        return left;
    }

    public MyBinaryTreeNode<E> getRight() {
        return right;
    } 

    public void setValue(E newVal) {
        value = newVal; 
    }

    public void setLeft(MyBinaryTreeNode<E> newLeftNode) {
        left = newLeftNode; 
    }

    public void setRight(MyBinaryTreeNode<E> newRightNode) {
        right = newRightNode; 
    } 
}
