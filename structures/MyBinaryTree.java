package uk.ac.warwick.cs126.structures;

import java.util.Comparator;

public class MyBinaryTree<E> {

    public MyBinaryTreeNode<E> root;
    public boolean foundNode; 
    public Comparator<E> comparator;

    public MyBinaryTree(Comparator<E> customComparisonFunction) {
        root = null; 
        foundNode = false;
        comparator = customComparisonFunction; 
    }

    /**
    * Method for adding a new node: Either adds to root or calls AddToSubTree to recursively locate correct pos. 
    * @param element the element to be added to the tree 
    */ 
    public void add(E element) {
        if (root == null) {
            //System.out.println("Adding root node " + element); 
            root = new MyBinaryTreeNode<>(element);
        } else {
            addToSubTree(root, element);
        }
    }

    /**
    * Method for adding a new node to the subtree of the given node - 
    * if the location that we try to add to (left or right subtree) is not nnull then we recursively call 
    * AddtoSubTree again with the left or right node as the new root node
    * @param node a MyBinaryTreeNode that we use as the root, whose subtree the new element is added
    * @param element the new element being added to a subtree of the passed root node
    */
    public void addToSubTree(MyBinaryTreeNode<E> node, E element) {
        if (node == null) {
            System.out.println("ERROR trying to add null node to tree"); 
        } else {
            E currentNodeVal = node.getValue();
            //if (element.compareTo(currentNodeVal) <= 0) {
            if (comparator.compare(element, currentNodeVal) <= 0) {
                if (node.getLeft() == null) {
                    //System.out.println("Left node is null so adding new node " + element);
                    node.setLeft(new MyBinaryTreeNode<>(element));
                } else {
                    addToSubTree(node.getLeft(), element); 
                }
            } else {
                if (node.getRight() == null) {
                    //System.out.println("right node is null so adding new node " + element);
                    
                    node.setRight(new MyBinaryTreeNode<>(element));
                } else {
                    addToSubTree(node.getRight(), element); 
                }
            } 
        }
    }

    /**
     * Recursively iterate through the tree and check if node is encountered which equals element
     * @param node starting node for the recursion 
     * @param element element to identify existense of 
     * @return boolean stating if found or not
     */
    public boolean contains(MyBinaryTreeNode<E> node, E element) {

        if (node != null) {
            //int equal = element.compareTo(node.getValue());
            int equal = comparator.compare(element, node.getValue()); 

            if (equal == 0) {
                return !node.isRemoved();
            }

            if (equal < 0 && node.getLeft() != null && contains(node.getLeft(), element)) {
                return true; 
            }

            if (equal > 0 && node.getRight() != null && contains(node.getRight(), element)) {
                return true; 
            }
        }
        return false;
    }

    public boolean findInTree(E element) {
        return contains(root, element);
    }

    public void remove(MyBinaryTreeNode<E> node, E element) {
        if (node != null) {
            if (node.getValue().equals(element) && !node.isRemoved()) {
                node.setRemoved(); 
            }
            remove(node.getLeft(), element);
            remove(node.getRight(), element); 
        } 
        

    }

    public void removeFromTree(E element) {
        if (root != null) {
            remove(root, element);
        }
    }

    public MyArrayList<E> inOrderTraversal() {
        MyArrayList<E> custIDs = new MyArrayList<>(); 
        inOrder(custIDs, root);
        return custIDs;

    }

    public void inOrder(MyArrayList<E> custIDs, MyBinaryTreeNode<E> node) {

        if (node != null) {
            inOrder(custIDs, node.getLeft());
            if (node.isRemoved() == false) {
                custIDs.add(node.getValue());
            }
            inOrder(custIDs, node.getRight());
        }
    }



    public boolean isEmpty() {
        return false;
    }

    public int size() {
        return 0; 
    }

    public String toString() {
        return ""; 
    }
}
