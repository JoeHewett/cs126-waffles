package uk.ac.warwick.cs126.structures;


/**
 * Code is based on code from Lab4 but has been modified to fulfil the necessary criteria for the Stores.
*/
public class MyKeyValuePairLinkedList<K extends Comparable<K>,V> {

    public int count; 
    protected MyListElement<MyKeyValuePair<K,V>> head;
    protected int size;
    
    public MyKeyValuePairLinkedList() {
        head = null;
        size = 0;
    }
    
    public void add(K key, V value) {
        this.add(new MyKeyValuePair<K,V>(key,value));
    }

    /**
     * Add an element to the linkedl ist
     * @param kvp the kvp object to add to the list 
     */
    public void add(MyKeyValuePair<K,V> kvp) {
        MyListElement<MyKeyValuePair<K,V>> new_element = new MyListElement<>(kvp);
        new_element.setNext(head);
        head = new_element;
        size++;
    }
    
    public int size() {
        return size;
    }
    
    public MyListElement<MyKeyValuePair<K,V>> getHead() {
        return head;
    }
    
    public MyKeyValuePair<K,V> get(K key) {
        MyListElement<MyKeyValuePair<K,V>> temp = head;
        
        while(temp != null) {
            count++;
            if(temp.getValue().getKey().equals(key)) {
                return temp.getValue();
            }
            
            temp = temp.getNext();
        }
        
        return null;
    }

    public void nullHead() {
        head = null; 
    }

    public MyArrayList<K> keys() {
        MyArrayList<K> listOfKeys = new MyArrayList<>(); 
        MyListElement<MyKeyValuePair<K,V>> temp = head; 

        while (temp != null) {
            listOfKeys.add(temp.getValue().getKey()); 
            temp = temp.getNext(); 
        }

        return listOfKeys; 

    }
    public MyArrayList<V> values() {
        MyArrayList<V> listOfValues = new MyArrayList<>(); 
        MyListElement<MyKeyValuePair<K,V>> temp = head; 

        while (temp != null) {
            listOfValues.add(temp.getValue().getValue()); 
            temp = temp.getNext(); 
        }

        return listOfValues; 

    }
}
