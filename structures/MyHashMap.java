package uk.ac.warwick.cs126.structures;

/**
 * Code is based on code from Lab4 but has been modified to fulfil the necessary criteria for the Stores.
*/

@SuppressWarnings("unchecked") 
public class MyHashMap<K extends Comparable<K>,V> {

    protected MyKeyValuePairLinkedList[] table;

    public MyHashMap() {

        table = new MyKeyValuePairLinkedList[1217];

        for(int i = 0; i < table.length; i++) {
            table[i] = new MyKeyValuePairLinkedList<>();
        }
    }
    
    protected int hash(K key) {
        int code = key.hashCode();
        return code;    
    }
    
    public boolean remove(K key) {
        int hash_code = hash(key);
        int location = Math.abs(hash_code % table.length);

        MyListElement<MyKeyValuePair<K, V>> ptr = table[location].getHead();
        if (ptr == null) {
            return false;
        }

        if (ptr.getValue().getKey() == key) {
            table[location].nullHead();
        }

        while (ptr.getNext() != null) {
            if (ptr.getNext().getValue().getKey() == key) {
                ptr.getNext().setPrev(ptr);
                ptr.setNext(ptr.getNext().getNext()); 
                 
                return true;
            }
            ptr = ptr.getNext();
        }
        return false;  

    }

    public void put(K key, V value) {
        if (key == null || value == null) {
            System.out.println("Adding null to hashmap ");
        } else {
            //System.out.println("adding valid object");
        }
        int hash_code = hash(key);
        int location = Math.abs(hash_code % table.length);
        table[location].add(key,value);
    }

    public V get(K key) {
        int hash_code = hash(key);
        int location = Math.abs(hash_code % table.length);

        try {
            return (V)table[location].get(key).getValue();
        } catch (Exception e) {
            return null; 
        }
    }

    public void replace(K key, V newVal) {
        int hash_code = hash(key);
        int location = Math.abs(hash_code % table.length);

        try {
            table[location].get(key).setValue(newVal);
        } catch (Exception e) {
            System.out.println("FAILED: Cannot replace key with new value");  
        }
    }

    public MyArrayList<K> toArrayListOfKeys() {
        // get array list of linked lists
        MyArrayList<K> tempList = new MyArrayList<>(); 
        MyArrayList<K> masterList = new MyArrayList<>();


        for (int i = 0; i < table.length; i++) {
            if (table[i] != null) {
                tempList = table[i].keys(); 
                for (int q = 0; q < tempList.size(); q++) {
                    masterList.add(tempList.get(q)); 
                }
            }
        }

        return masterList; 
    }

    public MyArrayList<V> toArrayListOfValues() {
        // get array list of linked lists
        MyArrayList<V> tempList = new MyArrayList<>(); 
        MyArrayList<V> masterList = new MyArrayList<>();


        for (int i = 0; i < table.length; i++) {
            if (table[i] != null) {
                tempList = table[i].values(); 
                for (int q = 0; q < tempList.size(); q++) {
                    masterList.add(tempList.get(q)); 
                }
            }
        }

        return masterList; 
    }
}
