package uk.ac.warwick.cs126.util;

public class HaversineDistanceCalculator {

    private final static float R = 6372.8f;
    private final static float kilometresInAMile = 1.609344f;

    /**
     * Calculate the distance between two points
     * @param lat1 lat of place 1
     * @param lon1 long of place 1
     * @param lat2 lat of place 2
     * @param lon2 long of place 2
     * @return
     */

    public static float inKilometresRaw(float lat1, float lon1, float lat2, float lon2) {
        double halfLatDist = Math.toRadians((lat2 - lat1) / 2);
        double halfLongDist = Math.toRadians((lon2 - lon1) / 2);

        lat1 = (float) Math.toRadians(lat1);
        lat2 = (float) Math.toRadians(lat1);

        double a = Math.pow(Math.sin(halfLatDist), 2)
                 + Math.cos(lat1)
                 * Math.cos(lat2)
                 * Math.pow(Math.sin(halfLongDist), 2);
        double c = 2 * Math.asin(Math.sqrt(a));

        float result = (float) (R * c * 10);
        return result;
    }

    public static float inKilometres(float lat1, float lon1, float lat2, float lon2) {
        float raw = inKilometresRaw(lat1, lon1, lat2, lon2);
        float roundedResult = (float) (Math.round(raw) / 10.0);
        return roundedResult;
    }

    public static float inMiles(float lat1, float lon1, float lat2, float lon2) {
        float raw = inKilometresRaw(lat1, lon1, lat2, lon2);
        float roundedResultInMiles = (float) (Math.round(raw / kilometresInAMile) / 10.0);
        return roundedResultInMiles;
    }

}