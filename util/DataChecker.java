package uk.ac.warwick.cs126.util;

import uk.ac.warwick.cs126.interfaces.IDataChecker;
import uk.ac.warwick.cs126.structures.MyHashMap;

import uk.ac.warwick.cs126.models.Customer;
import uk.ac.warwick.cs126.models.Restaurant;
import uk.ac.warwick.cs126.models.Favourite;
import uk.ac.warwick.cs126.models.Review;


public class DataChecker implements IDataChecker {

    public DataChecker() {
        // Initialise things here
    }

    /**
     * Takes an array of possible IDs and returns the one that is repeated twice or null else
     * @param repeatedID string array of the 3 possible IDs
     * @return Long of correct id or null else. 
     */
    public Long extractTrueID(String[] repeatedID) {

        if (repeatedID == null) {
            return null; 
        }

        MyHashMap<String, Integer> idhm = new MyHashMap<>();

        if (repeatedID.length != 3) {
            return null;
        } else {
            for (int i = 0; i < 3; i++) {
                
                if (idhm.get(repeatedID[i]) == null) {
                    idhm.put(repeatedID[i], 1);
                } else {
                    int num = idhm.get(repeatedID[i]) + 1;
                    idhm.replace(repeatedID[i], num);
                }
            }
            for (int i = 0; i < 3; i++) {
                if (idhm.get(repeatedID[i]) > 1) {
                    return Long.parseLong(repeatedID[i]);
                }
            }

        }
        return null;
    }

    /**
     * Takes an input ID and ensures it is valid to the spec
     * @param inputId the ID to check
     * @return boolean of validity 
     */
    public boolean isValid(Long inputID) {
        String inputString = Long.toString(inputID); 
        MyHashMap<Character, Integer> inputHashMap = new MyHashMap<>(); 

        if (inputString == null) {
            return false;
        }
        //check intput string is not null
        //check all chars at not ''0'
        // change to arrayLength not 16
        if (inputString.length() != 16) {
            return false;
        }

        for (int i = 0; i < inputString.length(); i++) {
            char c = inputString.charAt(i); 
            if ((c == '0' || c == '-')) {
                return false;
            } else {
                if (inputHashMap.get(c) == null) {
                        inputHashMap.put(c, 1);
                } else {
                    inputHashMap.replace(c, inputHashMap.get(c) + 1);

                }
            }
        }  

        for (int i = 0; i < inputString.length(); i++) {
            if (inputHashMap.get(inputString.charAt(i)) > 3) {
                return false;
            }
        }

        return true; 
    }

    /**
     * Check is the passed object is valid by checking nullity of its atributes
     * @param object to be checked
     * @return boolean depending on if validity holds based on criteria in spec
     */
    public boolean isValid(Customer customer) {
        
        if (customer != null &&
            isValid(customer.getID()) &&
            customer.getID() != null && 
            customer.getFirstName() != null && 
            customer.getLastName() != null &&
            customer.getDateJoined() != null) {
            return true; 
        }
        return false;
    }

   /**
     * Check is the passed object is valid by checking nullity of its atributes
     * @param object to be checked
     * @return boolean depending on if validity holds based on criteria in spec
     */    
    public boolean isValid(Restaurant restaurant) {
        if (restaurant == null) {
            return false;
        }

        if (isValid(restaurant.getID()) == false) {
            return false; 

        }
        int stars = restaurant.getWarwickStars();
        int rating = restaurant.getFoodInspectionRating(); 

        if (restaurant.getLastInspectedDate().compareTo(restaurant.getDateEstablished()) < 0) {
            return false;
        }

        if (restaurant != null &&
            restaurant.getID() != null &&
            restaurant.getRepeatedID() != null &&
            restaurant.getName() != null &&
            restaurant.getOwnerFirstName() != null &&
            restaurant.getOwnerLastName() != null &&
            restaurant.getCuisine() != null &&
            restaurant.getPriceRange() != null &&
            restaurant.getEstablishmentType() != null &&
            restaurant.getDateEstablished() != null &&

            (restaurant.getCustomerRating() == 0.0f || (restaurant.getCustomerRating() >= 1.0f && restaurant.getCustomerRating() <= 5.0f)) &&
            (stars == 0 || stars == 1 || stars == 2 || stars == 3) &&
            (rating == 0 || rating == 1 || rating == 2 || rating == 3 || rating == 4 || rating == 5)

        ) {
            return true;
        }    
            

        
        return false;
    }

       /**
     * Check is the passed object is valid by checking nullity of its atributes
     * @param object to be checked
     * @return boolean depending on if validity holds based on criteria in spec
     */
    public boolean isValid(Favourite favourite) {
        if (favourite != null &&
            isValid(favourite.getID()) &&
            favourite.getCustomerID() != null &&
            favourite.getRestaurantID() != null &&
            favourite.getDateFavourited() != null
            // favourite.getDateReviewed() != null &&
            // favourite.getReview() != null &&
            // favourite.getRating() != null

            ) {
                return true; 
            }
        return false;
    }

   /**
     * Check is the passed object is valid by checking nullity of its atributes
     * @param object to be checked
     * @return boolean depending on if validity holds based on criteria in spec
     */
    public boolean isValid(Review review) {
        if (review != null &&
            isValid(review.getID()) &&
            review.getRestaurantID() != null &&
            review.getDateReviewed() != null &&
            review.getReview() != null) {
            //review.getRating() != null) {
                return true; 
        }
        return false;
    }
}
