package uk.ac.warwick.cs126.stores;

import uk.ac.warwick.cs126.interfaces.IReviewStore;
import uk.ac.warwick.cs126.models.Review;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.io.IOUtils;

import uk.ac.warwick.cs126.structures.MyArrayList;
import uk.ac.warwick.cs126.structures.MyTuple;
import uk.ac.warwick.cs126.structures.QuickSorter;
import uk.ac.warwick.cs126.structures.MyHashMap;
import uk.ac.warwick.cs126.structures.MyHashSet;


import uk.ac.warwick.cs126.util.DataChecker;
import uk.ac.warwick.cs126.util.KeywordChecker;
import uk.ac.warwick.cs126.util.StringFormatter;

import java.util.HashMap;

import java.util.Date; 

import java.util.Comparator; 

public class ReviewStore implements IReviewStore {

    private DataChecker dataChecker;
    private KeywordChecker keywordChecker; 

    private MyHashSet<Long> blacklist; 
    private MyHashMap<Long, Review> reviewHashMap; 
    private MyHashMap<MyTuple<Long, Long>, Review> oldReviewHashMap; 

    private MyHashMap<Long, MyArrayList<Review>> reviewsByCustHashMap; 
    private MyHashMap<Long, MyArrayList<Review>> reviewsByRestHashMap; 

    public ReviewStore() {
        reviewHashMap = new MyHashMap<>(); 
        oldReviewHashMap = new MyHashMap<>(); 
        reviewsByCustHashMap = new MyHashMap<>(); 
        reviewsByRestHashMap = new MyHashMap<>(); 

        blacklist = new MyHashSet<>(); 

        dataChecker = new DataChecker();
    }

    public Review[] loadReviewDataToArray(InputStream resource) {
        Review[] reviewArray = new Review[0];

        try {
            byte[] inputStreamBytes = IOUtils.toByteArray(resource);
            BufferedReader lineReader = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(inputStreamBytes), StandardCharsets.UTF_8));

            int lineCount = 0;
            String line;
            while ((line = lineReader.readLine()) != null) {
                if (!("".equals(line))) {
                    lineCount++;
                }
            }
            lineReader.close();

            Review[] loadedReviews = new Review[lineCount - 1];

            BufferedReader tsvReader = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(inputStreamBytes), StandardCharsets.UTF_8));

            int reviewCount = 0;
            String row;
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

            tsvReader.readLine();
            while ((row = tsvReader.readLine()) != null) {
                if (!("".equals(row))) {
                    String[] data = row.split("\t");
                    Review review = new Review(
                            Long.parseLong(data[0]),
                            Long.parseLong(data[1]),
                            Long.parseLong(data[2]),
                            formatter.parse(data[3]),
                            data[4],
                            Integer.parseInt(data[5]));
                    loadedReviews[reviewCount++] = review;
                }
            }
            tsvReader.close();

            reviewArray = loadedReviews;

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }

        return reviewArray;
    }

    public boolean addReview(Review review) {
        // makea a tuple of reviewerID and RestID
        MyTuple<Long, Long> restAndCustID = new MyTuple<>(review.getCustomerID(), review.getRestaurantID()); 

        // first ensure new review is actually valid before we add it
        if (dataChecker.isValid(review)) {
            // proceed only if not in blacklist
            if (!blacklist.contains(review.getID())) {
                // if it does not exist in hashmap already, else add to blacklist and remove from hashmap 
                if (reviewHashMap.get(review.getID()) == null) {
                    // check if the cust has written a review for this rest already - if NOT then add to both hashmaps
                    if (oldReviewHashMap.get(restAndCustID) == null) {
                        addToHashMaps(review, restAndCustID);  
                        return true;
                    // if review with same custID, restID, then check that review added is newer before replacing. If not, do nothing. 
                    //} else if (review.getDateReviewed().compareTo(reviewHashMap.get(oldReviewHashMap.get(restAndCustID)).getDateReviewed()) > 0) {
                    } else if (review.getDateReviewed().compareTo(oldReviewHashMap.get(restAndCustID).getDateReviewed()) > 0) {    // get oldID and use it to remove old review and add to blacklist
                        Review oldReview = oldReviewHashMap.get(restAndCustID); 
                        reviewHashMap.remove(oldReview.getID()); 
                        oldReviewHashMap.remove(restAndCustID);
                        blacklist.add(oldReview.getID());

                        // once old review has been removed form both structures, add the new review to both.
                        reviewHashMap.put(review.getID(), review);
                        oldReviewHashMap.put(restAndCustID, review); 
                        return true;
                    }
                } else {
                    removeFromHashMaps(review, restAndCustID);
                }
            }
        }
        return false;
    }

    public void removeFromHashMaps(Review review, MyTuple<Long, Long> restAndCustID) {
        MyArrayList<Review> listOfReviewsByCustID;
        MyArrayList<Review> listOfReviewsByRestID;

        blacklist.add(review.getID());                    
        reviewHashMap.remove(review.getID()); 
        oldReviewHashMap.remove(restAndCustID);

        if (reviewsByCustHashMap.get(review.getCustomerID()) != null) {
            listOfReviewsByCustID = reviewsByCustHashMap.get(review.getCustomerID());
            listOfReviewsByCustID.remove(review); 
            reviewsByCustHashMap.replace(review.getCustomerID(), listOfReviewsByCustID);
        } 

        if (reviewsByRestHashMap.get(review.getRestaurantID()) != null) {
            listOfReviewsByRestID = reviewsByRestHashMap.get(review.getRestaurantID());
            listOfReviewsByRestID.remove(review); 
            reviewsByRestHashMap.replace(review.getRestaurantID(), listOfReviewsByRestID);
        } 
    }

    public void addToHashMaps(Review review, MyTuple<Long, Long> restAndCustID) {
        MyArrayList<Review> listOfReviewsByCustID; 
        MyArrayList<Review> listOfReviewsByRestID; 
        // add to the main store
        reviewHashMap.put(review.getID(), review);
        // add to auxillary store used to check for reviews with same custID, restID
        oldReviewHashMap.put(restAndCustID, review);

        // add to the hashMap that contains custID as key and ArrayList of reviews

        // if the customer already exists, append, else add new array withj ust 1 review
        if (reviewsByCustHashMap.get(review.getCustomerID()) != null) {
            listOfReviewsByCustID = reviewsByCustHashMap.get(review.getCustomerID());
            listOfReviewsByCustID.add(review); 
            reviewsByCustHashMap.replace(review.getCustomerID(), listOfReviewsByCustID);
        } else {
            listOfReviewsByCustID = new MyArrayList<Review>(); 
            listOfReviewsByCustID.add(review); 
            reviewsByCustHashMap.put(review.getCustomerID(), listOfReviewsByCustID);
        }

        if (reviewsByRestHashMap.get(review.getRestaurantID()) != null) {
            listOfReviewsByRestID = reviewsByRestHashMap.get(review.getRestaurantID());
            listOfReviewsByRestID.add(review); 
            reviewsByRestHashMap.replace(review.getRestaurantID(), listOfReviewsByRestID);
        } else {
            listOfReviewsByRestID = new MyArrayList<Review>(); 
            listOfReviewsByRestID.add(review); 
            reviewsByRestHashMap.put(review.getRestaurantID(), listOfReviewsByRestID);
        }

    }

    public boolean addReview(Review[] reviews) {
        boolean success = true; 
        for (int i = 0; i < reviews.length; i++) {
            if (!addReview(reviews[i])) {
                success = false; 
            }
        }
        return success;
    }

    public Review getReview(Long id) {
        return reviewHashMap.get(id);
    }

    public Review[] getReviews() {
        //reviews = reviewHashMap.values().toArray(new Review[0]); 
        MyArrayList<Review> reviewsList = reviewHashMap.toArrayListOfValues();
        Review[] reviews = new Review[reviewsList.size()]; 

        for (int i = 0; i < reviewsList.size(); i++) {
            reviews[i] = reviewsList.get(i);
        }

        QuickSorter<Review> qs = new QuickSorter<>();
        Review[] sortedReviews = qs.quickSort(reviews, 0, reviews.length - 1, new CompareReviewsID()); 
        return sortedReviews; 
    }

    public Review[] getReviewsByDate() {
        MyArrayList<Review> reviewsList = reviewHashMap.toArrayListOfValues();
        Review[] reviews = new Review[reviewsList.size()]; 

        for (int i = 0; i < reviewsList.size(); i++) {
            reviews[i] = reviewsList.get(i);
        }

        QuickSorter<Review> qs = new QuickSorter<>();
        Review[] sortedReviews = qs.quickSort(reviews, 0, reviews.length - 1, new CompareReviewsDate()); 
        return sortedReviews; 
    }

    public Review[] getReviewsByRating() {
        MyArrayList<Review> reviewsList = reviewHashMap.toArrayListOfValues();
        Review[] reviews = new Review[reviewsList.size()]; 

        for (int i = 0; i < reviewsList.size(); i++) {
            reviews[i] = reviewsList.get(i);
        }

        QuickSorter<Review> qs = new QuickSorter<>();
        Review[] sortedReviews = qs.quickSort(reviews, 0, reviews.length - 1, new CompareReviewsRating()); 
        return sortedReviews; 
    }

    public Review[] getReviewsByCustomerID(Long id) {
        MyArrayList<Review> listOfCustReviews = new MyArrayList<>(); 
        listOfCustReviews = reviewsByCustHashMap.get(id); 

        if (listOfCustReviews == null) {
            return new Review[0];
        }

        Review[] arrayOfReviews = new Review[listOfCustReviews.size()];

        for (int i = 0; i < arrayOfReviews.length; i++) {
            arrayOfReviews[i] = listOfCustReviews.get(i); 
        }

        QuickSorter<Review> qs = new QuickSorter<>();
        Review[] sortedReviews = qs.quickSort(arrayOfReviews, 0, arrayOfReviews.length - 1, new CompareReviewsDate()); 

        return sortedReviews; 

    }

    public Review[] getReviewsByRestaurantID(Long id) {
        MyArrayList<Review> listOfRestReviews = new MyArrayList<>(); 
        listOfRestReviews = reviewsByRestHashMap.get(id); 

        if (listOfRestReviews == null) {
            return new Review[0];
        }

        Review[] arrayOfReviews = new Review[listOfRestReviews.size()];

        for (int i = 0; i < arrayOfReviews.length; i++) {
            arrayOfReviews[i] = listOfRestReviews.get(i); 
        }

        QuickSorter<Review> qs = new QuickSorter<>();
        Review[] sortedReviews = qs.quickSort(arrayOfReviews, 0, arrayOfReviews.length - 1, new CompareReviewsDate()); 

        return sortedReviews; 
    }

    public float getAverageCustomerReviewRating(Long id) {
        float totalRating = 0.0f;
        int numReviews; 

        Review[] allCustomerReviews = getReviewsByCustomerID(id); 
        if (id == null || allCustomerReviews.length == 0) {
            return 0.0f;
        }

        numReviews = allCustomerReviews.length; 

        for (int i = 0; i < numReviews; i++) {
            totalRating += allCustomerReviews[i].getRating(); 
        }

        float total = (float) (Math.round((totalRating / numReviews * 10)) / 10.0);

        return total;
    }

    public float getAverageRestaurantReviewRating(Long id) {
        float totalRating = 0.0f;
        int numReviews; 

        Review[] allRestReviews = getReviewsByRestaurantID(id); 
        if (id == null || allRestReviews.length == 0) {
            return 0.0f;
        }

        numReviews = allRestReviews.length; 

        for (int i = 0; i < numReviews; i++) {
            totalRating += allRestReviews[i].getRating(); 
        }

        float total = (float) (Math.round((totalRating / numReviews * 10)) / 10.0);

        return total;
    }

    public int[] getCustomerReviewHistogramCount(Long id) {
        int[] histogram = new int[5];
        Review[] allReviews = getReviewsByCustomerID(id);

        for (int i = 0; i < allReviews.length; i++) {
            switch (allReviews[i].getRating()) {
                case 1: 
                    histogram[0]++; 
                    break; 
                case 2: 
                    histogram[1]++; 
                    break; 
                case 3: 
                    histogram[2]++; 
                    break; 
                case 4: 
                    histogram[3]++; 
                    break; 
                case 5: 
                    histogram[4]++; 
                    break; 
            }
        }

        return histogram; 
    }

    public int[] getRestaurantReviewHistogramCount(Long id) {
        int[] histogram = new int[5];
        Review[] allReviews = getReviewsByRestaurantID(id);

        for (int i = 0; i < allReviews.length; i++) {
            switch (allReviews[i].getRating()) {
                case 1: 
                    histogram[0]++; 
                    break; 
                case 2: 
                    histogram[1]++; 
                    break; 
                case 3: 
                    histogram[2]++; 
                    break; 
                case 4: 
                    histogram[3]++; 
                    break; 
                case 5: 
                    histogram[4]++; 
                    break; 
            }
        }

        return histogram; 
    }

    public Long[] getTopCustomersByReviewCount() {
        Long[] top20 = new Long[20]; 
        // iterate through keyset of reviewsByCustHashMap
        // Add tuple to array with new Tuple<Key, hashmap.get(key).size();> 
        MyArrayList<MyTuple<Long, MyTuple<Date, Integer>>> IDandTupleList = new MyArrayList<>(); 

        MyArrayList<Long> allCustKeys = reviewsByCustHashMap.toArrayListOfKeys();
        for (int i = 0; i < allCustKeys.size(); i++) {
            Long key = allCustKeys.get(i); 
            IDandTupleList.add(new MyTuple<>(key, new MyTuple<>(reviewsByCustHashMap.get(key).get(0).getDateReviewed(), reviewsByCustHashMap.get(key).size())));
        }
       
        // convert the array list to an array so it can be passed to quicksorter
        MyTuple<Long, MyTuple<Date, Integer>>[] idc = new MyTuple[IDandTupleList.size()];
        for (int i = 0; i < IDandTupleList.size(); i++) {
            idc[i] = IDandTupleList.get(i);
        }
        
        QuickSorter<MyTuple<Long, MyTuple<Date, Integer>>> qs = new QuickSorter<>();
        MyTuple<Long, MyTuple<Date, Integer>>[] sortedArray = qs.quickSort(idc, 0, idc.length - 1, new CompareReviewDateCount()); 

        for (int i = 0; i < 20; i++) {
            top20[i] = sortedArray[sortedArray.length - (i + 1)].getLeft(); 
        }

        return top20; 
    }

    public Long[] getTopRestaurantsByReviewCount() {
        Long[] top20 = new Long[20]; 
        // iterate through keyset of reviewsByCustHashMap
        // Add tuple to array with new Tuple<Key, hashmap.get(key).size();> 
        MyArrayList<MyTuple<Long, MyTuple<Date, Integer>>> IDandTupleList = new MyArrayList<>(); 

        // for (Long key : reviewsByRestHashMap.keySet()) {
        //     Date revDate = reviewsByRestHashMap.get(key).get(0).getDateReviewed();
        //     int size = reviewsByRestHashMap.get(key).size();

        //     IDandTupleList.add(new MyTuple<>(key, new MyTuple<>(revDate, size)));
        // }
        MyArrayList<Long> allRestKeys = reviewsByRestHashMap.toArrayListOfKeys();
        
        for (int i = 0; i < allRestKeys.size(); i++) {
            Long key = allRestKeys.get(i); 
            Date revDate = reviewsByRestHashMap.get(key).get(0).getDateReviewed();
            int size = reviewsByRestHashMap.get(key).size();

            IDandTupleList.add(new MyTuple<>(key, new MyTuple<>(revDate, size)));
        }

        // convert the array list to an array so it can be passed to quicksorter
        MyTuple<Long, MyTuple<Date, Integer>>[] idc = new MyTuple[IDandTupleList.size()];
        for (int i = 0; i < IDandTupleList.size(); i++) {
            idc[i] = IDandTupleList.get(i);
        }
        
        QuickSorter<MyTuple<Long, MyTuple<Date, Integer>>> qs = new QuickSorter<>();
        MyTuple<Long, MyTuple<Date, Integer>>[] sortedArray = qs.quickSort(idc, 0, idc.length - 1, new CompareReviewDateCount()); 

        for (int i = 0; i < 20; i++) {
            top20[i] = sortedArray[sortedArray.length - (i + 1)].getLeft(); 
        }

        return top20; 
    }
        
    public Long[] getTopRatedRestaurants() {
        Long[] top20 = new Long[20]; 
        // iterate through keyset of reviewsByCustHashMap
        // Add tuple to array with new Tuple<Key, hashmap.get(key).size();> 
        MyArrayList<MyTuple<Long, MyTuple<Date, Float>>> IDandTupleList = new MyArrayList<>(); 

        //for (Long key : reviewsByRestHashMap.keySet()) {
        MyArrayList<Long> allRestKeys = reviewsByRestHashMap.toArrayListOfKeys();
        for (int i = 0; i < allRestKeys.size(); i++) {
            Long key = allRestKeys.get(i);
            Date revDate = reviewsByRestHashMap.get(key).get(0).getDateReviewed();
            Float rating = getAverageRestaurantReviewRating(key);
            IDandTupleList.add(new MyTuple<>(key, new MyTuple<>(revDate, rating)));
        }
       
        // convert the array list to an array so it can be passed to quicksorter
        MyTuple<Long, MyTuple<Date, Float>>[] idc = new MyTuple[IDandTupleList.size()];
        for (int i = 0; i < IDandTupleList.size(); i++) {
            idc[i] = IDandTupleList.get(i);
        }
        
        QuickSorter<MyTuple<Long, MyTuple<Date, Float>>> qs = new QuickSorter<>();
        MyTuple<Long, MyTuple<Date, Float>>[] sortedArray = qs.quickSort(idc, 0, idc.length - 1, new CompareReviewRatingDateID()); 

        for (int i = 0; i < 20; i++) {
            top20[i] = sortedArray[sortedArray.length - (i + 1)].getLeft(); 
        }

        return top20; 
    }

    public String[] getTopKeywordsForRestaurant(Long id) {
        String[] top5 = new String[5];
        String allReviewsText = ""; 
        KeywordChecker kwc = new KeywordChecker();
        HashMap<String, Integer> keywordHashMap = new HashMap<>();

        if (reviewsByRestHashMap.get(id) == null) {
            return new String[5]; 
        }

        MyArrayList<Review> restReviews = reviewsByRestHashMap.get(id); 
        for (int i = 0; i < restReviews.size(); i++) {
            //System.out.println(restReviews.get(i).getReview() + " @@@@ " + restReviews.get(i).getRestaurantID());
            allReviewsText += " " + restReviews.get(i).getReview(); 
        }

        allReviewsText = allReviewsText.replaceAll("\\p{Punct}",""); 
        String[] splitWords = allReviewsText.split(" "); 
        for (int i = 0; i < splitWords.length; i++) {
            if (kwc.isAKeyword(splitWords[i])) {
                if (keywordHashMap.get(splitWords[i]) == null) {
                    keywordHashMap.put(splitWords[i], 1); 
                } else {
                    keywordHashMap.replace(splitWords[i], keywordHashMap.get(splitWords[i]) + 1); 
                }
            }
        }

        MyArrayList<MyTuple<String, Integer>> wordCountList = new MyArrayList<>(); 
        for (String key : keywordHashMap.keySet()) {
            wordCountList.add(new MyTuple<String, Integer>(key, keywordHashMap.get(key)));
        }

        MyTuple<String, Integer>[] wordCountArray = new MyTuple[wordCountList.size()];

        for (int i = 0; i < wordCountList.size(); i++) {
            wordCountArray[i] = wordCountList.get(i); 
        }
        
        QuickSorter<MyTuple<String, Integer>> qs = new QuickSorter<>();
        MyTuple<String, Integer>[] sortedArray = qs.quickSort(wordCountArray, 0, wordCountArray.length - 1, new CompareKeywordCount()); 

        
        for (int i = 0; i < 5; i++) {
            top5[i] = sortedArray[sortedArray.length - (i + 1)].getLeft(); 
        }

        return top5;
    }

    public Review[] getReviewsContaining(String searchTerm) {
        if (searchTerm == null || searchTerm == "") {
            return new Review[0];
        }                
        String searchTermConverted = StringFormatter.convertAccentsFaster(searchTerm).toLowerCase();

        Review[] allReviews = getReviews();
        MyArrayList<Review> matches = new MyArrayList<>();

        for (int i = 0; i < allReviews.length; i++) {
            String rev = allReviews[i].getReview();
            rev = rev.toLowerCase();

            if (rev.contains(searchTermConverted)) {
                matches.add(allReviews[i]);
            }
        }

        Review[] matchesArray = new Review[matches.size()];
        for (int x = 0; x < matches.size(); x++) {
            matchesArray[x] = matches.get(x);
        }

        QuickSorter<Review> qs = new QuickSorter<>();
        Review[] sortedArray = qs.quickSort(matchesArray, 0, matchesArray.length - 1, new CompareReviewDateID()); 

        return sortedArray;
    }
}

class CompareReviewDateID implements Comparator<Review> {
    public int compare(Review a, Review b) {
        int date = a.getDateReviewed().compareTo(b.getDateReviewed()); 
        int id = a.getID().compareTo(b.getID());

        if (date == 0) {
            return id; 
        } 
        return date; 
    }
}

class CompareKeywordCount implements Comparator<MyTuple<String, Integer>> {
    public int compare(MyTuple<String, Integer> a, MyTuple<String, Integer> b) {
        int count = Integer.compare(a.getRight(), b.getRight()); 
        int word = a.getLeft().compareTo(b.getLeft());
        word *= -1;  

        if (count == 0) {
            return word; 
        } 
        return count; 
    }
}

class CompareReviewsDate implements Comparator<Review> {
    @Override
    public int compare(Review a, Review b) {
        int date = a.getDateReviewed().compareTo(b.getDateReviewed()); 
        int id = a.getID().compareTo(b.getID());

        if (date == 0) {
            return id;
        } else {
            return date;
        }
    }

}

class CompareReviewsID implements Comparator<Review> {
    @Override
    public int compare(Review a, Review b) {
        return a.getID().compareTo(b.getID());
    }

}

class CompareReviewRatingDateID implements Comparator<MyTuple<Long, MyTuple<Date, Float>>> {
    @Override
    public int compare(MyTuple<Long, MyTuple<Date, Float>> a, MyTuple<Long, MyTuple<Date, Float>> b) {
        int rating =  Float.compare(a.getRight().getRight(), b.getRight().getRight());
        int id = a.getLeft().compareTo(b.getLeft()); 
        int date = a.getRight().getLeft().compareTo(b.getRight().getLeft());
        date *= -1;

        if (rating == 0) {
            if (date == 0) {
                return id;
            } else {
                return date; 
            }
        }
        return rating; 
    }

}

class CompareReviewDateCount implements Comparator<MyTuple<Long, MyTuple<Date, Integer>>> {
    @Override
    public int compare(MyTuple<Long, MyTuple<Date, Integer>> a, MyTuple<Long, MyTuple<Date, Integer>> b) {
        int count =  a.getRight().getRight().compareTo(b.getRight().getRight());
        int id = a.getLeft().compareTo(b.getLeft()); 
        int date = a.getRight().getLeft().compareTo(b.getRight().getLeft());
        date *= -1;

        if (count == 0) {
            if (date == 0) {
                return id;
            } else {
                return date; 
            }
        }
        return count; 
    }

}

class CompareRestDateAndCount implements Comparator<MyTuple<Long, MyTuple<Date, Integer>>> {
    @Override
    public int compare(MyTuple<Long, MyTuple<Date, Integer>> a, MyTuple<Long, MyTuple<Date, Integer>> b) {
        int count =  a.getRight().getRight().compareTo(b.getRight().getRight());
        int id = a.getLeft().compareTo(b.getLeft()); 
        int date = a.getRight().getLeft().compareTo(b.getRight().getLeft());

        if (count == 0) {
            if (date == 0) {
                return id;
            } else {
                return date; 
            }
        }
        return count; 
    }

}

class CompareReviewsRating implements Comparator<Review> {
    @Override
    public int compare(Review a, Review b) {
        int id = a.getID().compareTo(b.getID());
        int date = a.getDateReviewed().compareTo(b.getDateReviewed());
        int rating = Integer.compare(a.getRating(), b.getRating());

        if (rating == 0) {
            if (date == 0) {
                return id; 
            } else {
                return date;
            }
        }
        return rating; 
    }
    

}
