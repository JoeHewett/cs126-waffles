package uk.ac.warwick.cs126.stores;

import uk.ac.warwick.cs126.interfaces.IFavouriteStore;
import uk.ac.warwick.cs126.models.Favourite;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.io.IOUtils;

import uk.ac.warwick.cs126.structures.MyArrayList;
import uk.ac.warwick.cs126.structures.MyHashMap;
import uk.ac.warwick.cs126.structures.MyTuple;

import uk.ac.warwick.cs126.util.DataChecker;
import uk.ac.warwick.cs126.structures.QuickSorter;

import java.util.HashSet; 
import java.util.Date; 
import java.util.Comparator; 

public class FavouriteStore implements IFavouriteStore {

    private MyArrayList<Favourite> favouriteArray;
    private DataChecker dataChecker;

    private MyHashMap<Long, Favourite> favouriteHashMap; 

    private MyHashMap<Long, MyArrayList<Favourite>> favByRestHashMap; 
    private MyHashMap<Long, MyArrayList<Favourite>> favByCustHashMap;
    
    private MyHashMap<Long, Favourite> replacedHashMap; 
    private MyHashMap<MyTuple<Long, Long>, Favourite> oldFavHashMap;   
    
    private HashSet<Long> blacklist;

    public FavouriteStore() {

        // Initialise variables here
        favouriteArray = new MyArrayList<>();
        dataChecker = new DataChecker();

        favouriteHashMap = new MyHashMap<>(); 

        favByRestHashMap = new MyHashMap<>(); 
        favByCustHashMap = new MyHashMap<>(); 

        replacedHashMap = new MyHashMap<>(); 
        oldFavHashMap = new MyHashMap<>(); 

        blacklist = new HashSet<>(); 

    }

    public void removeFromHashMaps (Favourite fav, MyTuple<Long, Long> restAndCustID) {
        MyArrayList<Favourite> listOfFavByCustID; 
        MyArrayList<Favourite> listOfFavByRestID; 

        if (favByRestHashMap.get(fav.getRestaurantID()) != null) {
            listOfFavByRestID = favByRestHashMap.get(fav.getRestaurantID());
            listOfFavByRestID.remove(fav); 
            favByRestHashMap.replace(fav.getRestaurantID(), listOfFavByRestID);
        } 
    } 
    public void addToHashMaps(Favourite fav, MyTuple<Long, Long> restAndCustID) {
        MyArrayList<Favourite> listOfFavByCustID; 
        MyArrayList<Favourite> listOfFavByRestID; 

        favouriteHashMap.put(fav.getID(), fav);
        oldFavHashMap.put(restAndCustID, fav);

        if (favByCustHashMap.get(fav.getCustomerID()) != null) {
            listOfFavByCustID = favByCustHashMap.get(fav.getCustomerID());
            listOfFavByCustID.add(fav); 
            favByCustHashMap.replace(fav.getCustomerID(), listOfFavByCustID);
        } else {
            listOfFavByCustID = new MyArrayList<Favourite>(); 
            listOfFavByCustID.add(fav); 
            favByCustHashMap.put(fav.getCustomerID(), listOfFavByCustID);
        }

        if (favByRestHashMap.get(fav.getRestaurantID()) != null) {
            listOfFavByRestID = favByRestHashMap.get(fav.getRestaurantID());
            listOfFavByRestID.add(fav); 
            favByRestHashMap.replace(fav.getRestaurantID(), listOfFavByRestID);
        } else {
            listOfFavByRestID = new MyArrayList<Favourite>(); 
            listOfFavByRestID.add(fav); 
            favByRestHashMap.put(fav.getRestaurantID(), listOfFavByRestID);
        }

    }    

    public boolean addFavourite(Favourite favourite) {
        MyTuple<Long, Long> restAndCustID = new MyTuple<>(favourite.getCustomerID(), favourite.getRestaurantID());
        if (!(dataChecker.isValid(favourite))) {
            return false;
        }

        Long id = favourite.getID();
        if (blacklist.contains(id)) {
            return false; 
        } 

        if (favouriteHashMap.get(id) == null) {
            if (oldFavHashMap.get(restAndCustID) == null) {
                addToHashMaps(favourite, restAndCustID); 
                return true; 
            } else {
                if (favourite.getDateFavourited().compareTo(oldFavHashMap.get(restAndCustID).getDateFavourited()) > 0) {
                //if (existingFav.compareTo(proposedFav) > 0) {
                    blacklist.add(oldFavHashMap.get(restAndCustID).getID()); 
                    replacedHashMap.put(id, oldFavHashMap.get(restAndCustID)); 
                    // and add new one to all hashmaps 
                    addToHashMaps(favourite, restAndCustID);
                    removeFromHashMaps(oldFavHashMap.get(restAndCustID), restAndCustID); 
                    return true;
                } else {
                    replacedHashMap.put(oldFavHashMap.get(restAndCustID).getID(), favourite);  
                }
            }
        // id already exists in favouriteHashMap so blacklist, but check if this means we can reinstate something.
        } else {
            blacklist.add(id);
            removeFromHashMaps(favourite, restAndCustID); 
            if (replacedHashMap.get(id) != null) {
                Favourite replaced = replacedHashMap.get(id); 
                // reinstate the one that got replaced
                // TODO: add review from replacedHashMap to all hashmaps 
                addToHashMaps(replaced, restAndCustID); 
                replacedHashMap.remove(id); 
                blacklist.remove(replaced.getID()); 
            }
        }

        
        
        // check if vlaid 
            // check if !blacklisted

                // check if revID !exists in store
                    //  if CustRestID already exists in the store then 
                            // check the dates of newFav and oldFav
                            // add oldFav to store, blacklist newFav
                            // add newFav to replacedHM
                            
                        // else 
                            // simply add the new Favourite 
                // else 
                    // blacklist ID, remove rev with same ID from the store
                    // 
                    // if tuple exists in replaced and then
                        // reinstate old 
                        // remove id from blacklist 
                        // remove tuple from replacedHashMaps. 
        return false;
    }

    public boolean addFavourite(Favourite[] favourites) {
        boolean success = true; 
        for (int i = 0; i < favourites.length; i++) {
            if (!addFavourite(favourites[i])) {
                success = false; 
            }
        }
        return success;
    }

    public Favourite getFavourite(Long id) {
        return favouriteHashMap.get(id); 
    }

    public Favourite[] getFavourites() {
        MyArrayList<Favourite> favList = favouriteHashMap.toArrayListOfValues();
        Favourite[] favs = new Favourite[favList.size()];
        
        for (int i = 0; i < favList.size(); i++) {
            favs[i] = favList.get(i); 
        }

        QuickSorter<Favourite> qs = new QuickSorter<>();
        Favourite[] sortedFavs = qs.quickSort(favs, 0, favs.length - 1, new CompareFavID()); 
        return sortedFavs;
    }

    public Favourite[] getFavouritesByCustomerID(Long id) {
        MyArrayList<Favourite> listOfCustFavs = new MyArrayList<>(); 
        listOfCustFavs = favByCustHashMap.get(id); 

        if (listOfCustFavs == null) {
            return new Favourite[0];
        }

        Favourite[] arrayOfFavs = new Favourite[listOfCustFavs.size()];

        for (int i = 0; i < arrayOfFavs.length; i++) {
            arrayOfFavs[i] = listOfCustFavs.get(i); 
        }

        QuickSorter<Favourite> qs = new QuickSorter<>();
        Favourite[] sortedReviews = qs.quickSort(arrayOfFavs, 0, arrayOfFavs.length - 1, new CompareFavDateID()); 

        return sortedReviews; 
    }

    public Favourite[] getFavouritesByRestaurantID(Long id) {
        MyArrayList<Favourite> listOfRestFavs = new MyArrayList<>(); 
        listOfRestFavs = favByRestHashMap.get(id); 

        if (listOfRestFavs == null) {
            return new Favourite[0];
        }

        Favourite[] arrayOfFavs = new Favourite[listOfRestFavs.size()];

        for (int i = 0; i < arrayOfFavs.length; i++) {
            arrayOfFavs[i] = listOfRestFavs.get(i); 
        }

        QuickSorter<Favourite> qs = new QuickSorter<>();
        Favourite[] sortedReviews = qs.quickSort(arrayOfFavs, 0, arrayOfFavs.length - 1, new CompareFavDateID()); 

        return sortedReviews; 
    }

    public Long[] getCommonFavouriteRestaurants(Long customer1ID, Long customer2ID) {
         
        Favourite[] cust1Favs = getFavouritesByCustomerID(customer1ID); 
        Favourite[] cust2Favs = getFavouritesByCustomerID(customer2ID);
        
        HashSet<Favourite> cust1FavsSet = new HashSet<>(); 
        MyArrayList<Favourite> commonFavsList = new MyArrayList<>(); 

        for (int i = 0; i < cust1Favs.length; i++) {
            cust1FavsSet.add(cust1Favs[i]);
        }

        for (int x = 0; x < cust2Favs.length; x++) {
            if (cust1FavsSet.contains(cust2Favs[x])) {
                commonFavsList.add(cust2Favs[x]); 
            }
        }

        Favourite[] commonFavsArray = new Favourite[commonFavsList.size()];

        for (int s = 0; s < commonFavsArray.length; s++) {
            commonFavsArray[s] = commonFavsList.get(s);  
        }

        QuickSorter<Favourite> qs = new QuickSorter<>();
        Favourite[] sortedReviews = qs.quickSort(commonFavsArray, 0, commonFavsArray.length - 1, new CompareFavDateID()); 

        Long[] commonRestIDArray = new Long[sortedReviews.length];
        for (int i = 0; i < commonRestIDArray.length; i++) {
            commonRestIDArray[i] = sortedReviews[i].getRestaurantID(); 
        }

        return commonRestIDArray; 

    }

    public Long[] getNotCommonFavouriteRestaurants(Long customer1ID, Long customer2ID) {
        Favourite[] cust1Favs = getFavouritesByCustomerID(customer1ID); 
        Favourite[] cust2Favs = getFavouritesByCustomerID(customer2ID);
        
        HashSet<Favourite> cust1FavsSet = new HashSet<>(); 
        HashSet<Favourite> cust2FavsSet = new HashSet<>(); 

        MyArrayList<Favourite> cust1NotCust2FavsList = new MyArrayList<>(); 
        MyArrayList<Favourite> cust2NotCust1FavsList = new MyArrayList<>(); 

        for (int i = 0; i < cust1Favs.length; i++) {
            cust1FavsSet.add(cust1Favs[i]);
        }

        for (int i = 0; i < cust2Favs.length; i++) {
            cust2FavsSet.add(cust2Favs[i]);
        }

        for (int x = 0; x < cust2Favs.length; x++) {
            if (!(cust1FavsSet.contains(cust2Favs[x]))) {
                cust2NotCust1FavsList.add(cust2Favs[x]); 
            }
        }

        for (int x = 0; x < cust1Favs.length; x++) {
            if (!(cust2FavsSet.contains(cust1Favs[x]))) {
                cust1NotCust2FavsList.add(cust1Favs[x]); 
            }
        }

        MyArrayList<Favourite> wholeList = cust1NotCust2FavsList;
        for (int i = 0; i < cust2NotCust1FavsList.size(); i++) {
            wholeList.add(cust2NotCust1FavsList.get(i)); 
        }

        Favourite[] excluseFavsArray = new Favourite[wholeList.size()];

        for (int s = 0; s < wholeList.size(); s++) {
            excluseFavsArray[s] = wholeList.get(s);  
        }

        QuickSorter<Favourite> qs = new QuickSorter<>();
        Favourite[] sortedReviews = qs.quickSort(excluseFavsArray, 0, excluseFavsArray.length - 1, new CompareFavDateID()); 

        Long[] commonRestIDArray = new Long[sortedReviews.length];
        for (int i = 0; i < commonRestIDArray.length; i++) {
            commonRestIDArray[i] = sortedReviews[i].getRestaurantID(); 
        }

        return commonRestIDArray; 
    }

    public Long[] getMissingFavouriteRestaurants(Long customer1ID, Long customer2ID) {
        Favourite[] cust1Favs = getFavouritesByCustomerID(customer1ID); 
        Favourite[] cust2Favs = getFavouritesByCustomerID(customer2ID);
        
        HashSet<Favourite> cust2FavsSet = new HashSet<>(); 
        MyArrayList<Favourite> exclusiveTo1 = new MyArrayList<>(); 

        for (int i = 0; i < cust2Favs.length; i++) {
            cust2FavsSet.add(cust2Favs[i]);
        }

        for (int x = 0; x < cust1Favs.length; x++) {
            if (cust2FavsSet.contains(cust1Favs[x]) == false) {
                exclusiveTo1.add(cust1Favs[x]); 
            }
        }

        Favourite[] commonFavsArray = new Favourite[exclusiveTo1.size()];

        for (int s = 0; s < commonFavsArray.length; s++) {
            commonFavsArray[s] = exclusiveTo1.get(s);  
        }

        QuickSorter<Favourite> qs = new QuickSorter<>();
        Favourite[] sortedReviews = qs.quickSort(commonFavsArray, 0, commonFavsArray.length - 1, new CompareFavDateID()); 

        Long[] commonRestIDArray = new Long[sortedReviews.length];
        for (int i = 0; i < commonRestIDArray.length; i++) {
            commonRestIDArray[i] = sortedReviews[i].getRestaurantID(); 
        }

        return commonRestIDArray; 
    }

    public Long[] getTopCustomersByFavouriteCount() {
        Long[] top20 = new Long[20]; 
        MyArrayList<MyTuple<Long, MyTuple<Date, Integer>>> IDandTupleList = new MyArrayList<>(); 

        MyArrayList<Long> allRestKeys = favByCustHashMap.toArrayListOfKeys();
        
        for (int i = 0; i < allRestKeys.size(); i++) {
            Long key = allRestKeys.get(i); 
            Date revDate = favByCustHashMap.get(key).get(0).getDateFavourited();
            int size = favByCustHashMap.get(key).size();

            IDandTupleList.add(new MyTuple<>(key, new MyTuple<>(revDate, size)));
        }

        // convert the array list to an array so it can be passed to quicksorter
        MyTuple<Long, MyTuple<Date, Integer>>[] idc = new MyTuple[IDandTupleList.size()];
        for (int i = 0; i < IDandTupleList.size(); i++) {
            idc[i] = IDandTupleList.get(i);
        }
        
        QuickSorter<MyTuple<Long, MyTuple<Date, Integer>>> qs = new QuickSorter<>();
        MyTuple<Long, MyTuple<Date, Integer>>[] sortedArray = qs.quickSort(idc, 0, idc.length - 1, new CompareFavCountDateID()); 

        for (int i = 0; i < 20; i++) {
            top20[i] = sortedArray[sortedArray.length - (i + 1)].getLeft(); 
        }

        return top20; 
    }

    public Long[] getTopRestaurantsByFavouriteCount() {
        Long[] top20 = new Long[20]; 
        MyArrayList<MyTuple<Long, MyTuple<Date, Integer>>> IDandTupleList = new MyArrayList<>(); 

        MyArrayList<Long> allRestKeys = favByRestHashMap.toArrayListOfKeys();
        
        for (int i = 0; i < allRestKeys.size(); i++) {
            Long key = allRestKeys.get(i); 
            Date revDate = favByRestHashMap.get(key).get(0).getDateFavourited();
            int size = favByRestHashMap.get(key).size();

            IDandTupleList.add(new MyTuple<>(key, new MyTuple<>(revDate, size)));
        }

        // convert the array list to an array so it can be passed to quicksorter
        MyTuple<Long, MyTuple<Date, Integer>>[] idc = new MyTuple[IDandTupleList.size()];
        for (int i = 0; i < IDandTupleList.size(); i++) {
            idc[i] = IDandTupleList.get(i);
        }
        
        QuickSorter<MyTuple<Long, MyTuple<Date, Integer>>> qs = new QuickSorter<>();
        MyTuple<Long, MyTuple<Date, Integer>>[] sortedArray = qs.quickSort(idc, 0, idc.length - 1, new CompareFavCountDateID()); 

        for (int i = 0; i < 20; i++) {
            top20[i] = sortedArray[sortedArray.length - (i + 1)].getLeft(); 
        }

        return top20; 
    }

    public Favourite[] loadFavouriteDataToArray(InputStream resource) {
        Favourite[] favouriteArray = new Favourite[0];

        try {
            byte[] inputStreamBytes = IOUtils.toByteArray(resource);
            BufferedReader lineReader = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(inputStreamBytes), StandardCharsets.UTF_8));

            int lineCount = 0;
            String line;
            while ((line = lineReader.readLine()) != null) {
                if (!("".equals(line))) {
                    lineCount++;
                }
            }
            lineReader.close();

            Favourite[] loadedFavourites = new Favourite[lineCount - 1];

            BufferedReader csvReader = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(inputStreamBytes), StandardCharsets.UTF_8));

            int favouriteCount = 0;
            String row;
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

            csvReader.readLine();
            while ((row = csvReader.readLine()) != null) {
                if (!("".equals(row))) {
                    String[] data = row.split(",");
                    Favourite favourite = new Favourite(
                            Long.parseLong(data[0]),
                            Long.parseLong(data[1]),
                            Long.parseLong(data[2]),
                            formatter.parse(data[3]));
                    loadedFavourites[favouriteCount++] = favourite;
                }
            }
            csvReader.close();

            favouriteArray = loadedFavourites;

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }

        return favouriteArray;
    }
}

class CompareFavID implements Comparator<Favourite> {
    public int compare(Favourite a, Favourite b) {
        return a.getID().compareTo(b.getID()); 
    }
}

class CompareFavDateID implements Comparator<Favourite> {
    public int compare(Favourite a, Favourite b) {
        int date = a.getDateFavourited().compareTo(b.getDateFavourited()); 
        int id = a.getID().compareTo(b.getID()); 

        if (date == 0) {
            return id; 
        } else {
            return date; 
        }
    }
}

class CompareFavCountDateID implements Comparator<MyTuple<Long, MyTuple<Date, Integer>>> {
    @Override
    public int compare(MyTuple<Long, MyTuple<Date, Integer>> a, MyTuple<Long, MyTuple<Date, Integer>> b) {
        int count =  a.getRight().getRight().compareTo(b.getRight().getRight());
        int id = a.getLeft().compareTo(b.getLeft()); 
        int date = a.getRight().getLeft().compareTo(b.getRight().getLeft());

        if (count == 0) {
            if (date == 0) {
                return id;
            } else {
                return date; 
            }
        }
        return count; 
    }

}

