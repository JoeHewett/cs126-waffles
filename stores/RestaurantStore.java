package uk.ac.warwick.cs126.stores;

import uk.ac.warwick.cs126.interfaces.IRestaurantStore;
import uk.ac.warwick.cs126.models.Cuisine;
import uk.ac.warwick.cs126.models.EstablishmentType;
import uk.ac.warwick.cs126.models.Place;
import uk.ac.warwick.cs126.models.PriceRange;
import uk.ac.warwick.cs126.models.Restaurant;
import uk.ac.warwick.cs126.models.RestaurantDistance;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.io.IOUtils;
import org.omg.PortableServer.IdAssignmentPolicy;

import uk.ac.warwick.cs126.structures.MyArrayList;
import uk.ac.warwick.cs126.structures.MyBinaryTree;
import uk.ac.warwick.cs126.structures.MyHashMap; 
import uk.ac.warwick.cs126.structures.MyHashSet; 
import uk.ac.warwick.cs126.structures.QuickSorter; 
 
import uk.ac.warwick.cs126.util.ConvertToPlace;
import uk.ac.warwick.cs126.util.HaversineDistanceCalculator;
import uk.ac.warwick.cs126.util.DataChecker;
import uk.ac.warwick.cs126.util.StringFormatter;
import uk.ac.warwick.cs126.util.ConvertToPlace;


import java.util.Comparator; 

import java.util.Date;

public class RestaurantStore implements IRestaurantStore {

    private DataChecker dataChecker;
    private ConvertToPlace convertToPlace; 

    private MyBinaryTree<Long> restNameTree;
    private MyBinaryTree<Long> restIDTree;
    private MyBinaryTree<Long> restDateTree;

    private MyBinaryTree<Restaurant> restStarsTree;
    private MyBinaryTree<RestaurantDistance> restDistTree; 

    private MyHashMap<Long, Restaurant> restHashMap; 
    private MyHashSet<Long> blacklist;

    private CompareRestaurantNameID restNameIDComparator;
    private CompareRestaurantDateNameIDlong restDateNameIDComparator;
    
    public RestaurantStore() {
        // Initialise variables here
        dataChecker = new DataChecker();
        convertToPlace = new ConvertToPlace(); 


        restIDTree = new MyBinaryTree<Long>(new CompareLongs()); 
        restStarsTree = new MyBinaryTree<Restaurant>(new CompareRestaurantStars()); 

        restHashMap = new MyHashMap<>(); 
        blacklist = new MyHashSet<>(); 

        restNameIDComparator = new CompareRestaurantNameID(restHashMap); 
        restDateNameIDComparator = new CompareRestaurantDateNameIDlong(); 
        restNameTree = new MyBinaryTree<Long>(restNameIDComparator);
        restDateTree = new MyBinaryTree<Long>(restDateNameIDComparator);


    }

    public Restaurant[] loadRestaurantDataToArray(InputStream resource) {
        Restaurant[] restaurantArray = new Restaurant[0];

        try {
            byte[] inputStreamBytes = IOUtils.toByteArray(resource);
            BufferedReader lineReader = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(inputStreamBytes), StandardCharsets.UTF_8));

            int lineCount = 0;
            String line;
            while ((line = lineReader.readLine()) != null) {
                if (!("".equals(line))) {
                    lineCount++;
                }
            }
            lineReader.close();
            Restaurant[] loadedRestaurants = new Restaurant[lineCount - 1];

            BufferedReader csvReader = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(inputStreamBytes), StandardCharsets.UTF_8));

            String row;
            int restaurantCount = 0;
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

            csvReader.readLine();
            while ((row = csvReader.readLine()) != null) {
                if (!("".equals(row))) {
                    String[] data = row.split(",");

                    Restaurant restaurant = new Restaurant(
                            data[0],
                            data[1],
                            data[2],
                            data[3],
                            Cuisine.valueOf(data[4]),
                            EstablishmentType.valueOf(data[5]),
                            PriceRange.valueOf(data[6]),
                            formatter.parse(data[7]),
                            Float.parseFloat(data[8]),
                            Float.parseFloat(data[9]),
                            Boolean.parseBoolean(data[10]),
                            Boolean.parseBoolean(data[11]),
                            Boolean.parseBoolean(data[12]),
                            Boolean.parseBoolean(data[13]),
                            Boolean.parseBoolean(data[14]),
                            Boolean.parseBoolean(data[15]),
                            formatter.parse(data[16]),
                            Integer.parseInt(data[17]),
                            Integer.parseInt(data[18]));

                    loadedRestaurants[restaurantCount++] = restaurant;
                }
            }
            csvReader.close();

            restaurantArray = loadedRestaurants;

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }

        return restaurantArray;
    }

    public boolean addRestaurant(Restaurant restaurant) {
        updateHashMaps(); 
        Long id = dataChecker.extractTrueID(restaurant.getRepeatedID()); 
        restaurant.setID(id); 

        if (id == null) {
            return false; 
        }

        if (dataChecker.isValid(restaurant)) {
            if (!blacklist.contains(id)) {
                if (restHashMap.get(id) == null) {
                    restHashMap.put(id, restaurant);
                    updateHashMaps(); 

                    restNameTree.add(id); 
                    restIDTree.add(id);
                    restDateTree.add(id); 
                    if (restaurant.getWarwickStars() > 0) {restStarsTree.add(restaurant);}
                    
                    return true; 
                } else {
                    blacklist.add(id);
                    restNameTree.removeFromTree(id);
                    restIDTree.removeFromTree(id);
                    restDateTree.removeFromTree(id);
                    restStarsTree.removeFromTree(restaurant);
                    restHashMap.remove(id);
                    updateHashMaps(); 
                }    
            }
        }

        return false;
    }

    public void updateHashMaps() {
        restNameIDComparator.passHashMap(restHashMap); 
        restDateNameIDComparator.passHashMap(restHashMap); 
    }

    public boolean addRestaurant(Restaurant[] restaurants) {
        boolean success = true;
        for (int i = 0; i < restaurants.length; i++) {
            if (!addRestaurant(restaurants[i])) {
                success = false;
            }
        }

        return success;
    }

    public Restaurant getRestaurant(Long id) {
        return restHashMap.get(id);
    }

    public Restaurant[] getRestaurants() {
        MyArrayList<Long> restIDs = new MyArrayList<>();
        restIDs = restIDTree.inOrderTraversal();

        Restaurant[] orderedRestaurantObjs = new Restaurant[restIDs.size()];

        for (int i = 0; i < orderedRestaurantObjs.length; i++) {
            //orderedRestaurantObjs[i] = restHashMap.get(restIDs.get(i));
            orderedRestaurantObjs[i] = getRestaurant(restIDs.get(i)); 
        }

        return orderedRestaurantObjs; 
    }

    public Restaurant[] getRestaurants(Restaurant[] restaurants) {
        QuickSorter<Restaurant> qs = new QuickSorter<>();
        return qs.quickSort(restaurants, 0, restaurants.length - 1, new CompareRestaurantID()); 
    }

    public Restaurant[] getRestaurantsByName() {
        
        MyArrayList<Long> restIDs = new MyArrayList<>();
        restIDs = restNameTree.inOrderTraversal();

        Restaurant[] orderedRestaurantObjs = new Restaurant[restIDs.size()];

        for (int i = 0; i < orderedRestaurantObjs.length; i++) {
            orderedRestaurantObjs[i] = getRestaurant(restIDs.get(i)); 
        }

        return orderedRestaurantObjs; 
    }

    public Restaurant[] getRestaurantsByDateEstablished() {
        MyArrayList<Long> rests = new MyArrayList<>();
        rests = restDateTree.inOrderTraversal();

        Restaurant[] restsArray = new Restaurant[rests.size()];

        for (int i = 0; i < restsArray.length; i++) {
            restsArray[i] = getRestaurant(rests.get(i));
        }

        return restsArray; 

    }

    public Restaurant[] getRestaurantsByDateEstablished(Restaurant[] restaurants) {
        QuickSorter<Restaurant> qs = new QuickSorter<>();
        return qs.quickSort(restaurants, 0, restaurants.length - 1, new CompareRestaurantDateNameIDrest()); 
    }

    public Restaurant[] getRestaurantsByWarwickStars() {
        MyArrayList<Restaurant> rests = new MyArrayList<>();
        rests = restStarsTree.inOrderTraversal();

        Restaurant[] restsArray = new Restaurant[rests.size()];

        for (int i = 0; i < restsArray.length; i++) {
            restsArray[i] = rests.get(i);
        }

        return restsArray; 
    }

    public Restaurant[] getRestaurantsByRating(Restaurant[] restaurants) {
        QuickSorter<Restaurant> qs = new QuickSorter<>();
        return qs.quickSort(restaurants, 0, restaurants.length - 1, new CompareRating()); 
    }

    public RestaurantDistance[] getRestaurantsByDistanceFrom(float latitude, float longitude) {
        
        restDistTree = new MyBinaryTree<RestaurantDistance>(new CompareRestaurantDistance()); 
        MyArrayList<RestaurantDistance> orderedRestDists;
        RestaurantDistance[] orderedRestDistsArray; 

        Restaurant[] allRestaurants = getRestaurants(); 
        RestaurantDistance restDistObj;

        if (allRestaurants.length == 0) {
            return new RestaurantDistance[0]; 
        }

        for (int i = 0; i < allRestaurants.length; i++) {
            restDistObj = new RestaurantDistance(allRestaurants[i], HaversineDistanceCalculator.inKilometres(latitude, longitude, allRestaurants[i].getLatitude(), allRestaurants[i].getLongitude())); 
            restDistTree.add(restDistObj);
        }

        orderedRestDists = restDistTree.inOrderTraversal(); 
        orderedRestDistsArray = new RestaurantDistance[orderedRestDists.size()];

        for (int i = 0; i < orderedRestDistsArray.length; i++) {
            orderedRestDistsArray[i] = orderedRestDists.get(i);
        }
        
        return orderedRestDistsArray;
    }

    public RestaurantDistance[] getRestaurantsByDistanceFrom(Restaurant[] restaurants, float latitude, float longitude) {
        restDistTree = new MyBinaryTree<RestaurantDistance>(new CompareRestaurantDistance()); 
        MyArrayList<RestaurantDistance> orderedRestDists;
        RestaurantDistance[] orderedRestDistsArray; 

        RestaurantDistance restDistObj;

        if (restaurants.length == 0) {
            return new RestaurantDistance[0]; 
        }

        for (int i = 0; i < restaurants.length; i++) {
            restDistObj = new RestaurantDistance(restaurants[i], HaversineDistanceCalculator.inKilometres(latitude, longitude, restaurants[i].getLatitude(), restaurants[i].getLongitude())); 
            restDistTree.add(restDistObj);
        }

        orderedRestDists = restDistTree.inOrderTraversal(); 
        orderedRestDistsArray = new RestaurantDistance[orderedRestDists.size()];

        for (int i = 0; i < orderedRestDistsArray.length; i++) {
            orderedRestDistsArray[i] = orderedRestDists.get(i);
        }
        
        return orderedRestDistsArray;
    }

    public Restaurant[] getRestaurantsContaining(String searchTerm) {
        if (searchTerm == null || searchTerm == "") {
            return new Restaurant[0];
        }                

        String searchTermConverted = StringFormatter.convertAccentsFaster(searchTerm).toLowerCase();
        String[] formattedSearchTermArray = searchTermConverted.split("\\s+");
        String formattedSearchTerm = ""; 

        for (int i = 0; i < formattedSearchTermArray.length; i ++) {
            formattedSearchTerm += formattedSearchTermArray[i];
            if (i != formattedSearchTermArray.length -1) {
                formattedSearchTerm += " ";
            }
        }

        formattedSearchTerm = formattedSearchTerm.toLowerCase(); 

        Restaurant[] allRestaurants = getRestaurantsByName();
        MyArrayList<Restaurant> matches = new MyArrayList<>();

        for (int i = 0; i < allRestaurants.length; i++) {
            String name = allRestaurants[i].getName().toLowerCase();
            String cuisineName = allRestaurants[i].getCuisine().toString().toLowerCase(); 
            String placeName = convertToPlace.convert(allRestaurants[i].getLatitude(), allRestaurants[i].getLongitude()).toString().toLowerCase(); 
 

            if (name.contains(formattedSearchTerm) ||
                cuisineName.contains(formattedSearchTerm) ||
                placeName.contains(formattedSearchTerm)) {
                    matches.add(allRestaurants[i]);
            }
        }

        Restaurant[] matchesArray = new Restaurant[matches.size()];
        for (int x = 0; x < matches.size(); x++) {
            matchesArray[x] = matches.get(x);
        }

        return matchesArray;
    }
}


class CompareRestaurantDistance implements Comparator<RestaurantDistance> {
    @Override
    public int compare(RestaurantDistance rd1, RestaurantDistance rd2) {
        int id = rd1.getRestaurant().getID().compareTo(rd2.getRestaurant().getID());
        int dist = Float.compare(rd1.getDistance(), rd2.getDistance());

        if (dist == 0) {
            return id;
        } else {
            return dist;
        }
    }

}

class CompareRestaurantNameID implements Comparator<Long> {
    MyHashMap<Long, Restaurant> restHM; 

    public CompareRestaurantNameID(MyHashMap<Long, Restaurant> hm) {
        restHM = hm; 
    }

    @Override
    public int compare(Long id1, Long id2) {

        Restaurant rest1 = restHM.get(id1);
        Restaurant rest2 = restHM.get(id2);

        int name = rest1.getName().compareTo(rest2.getName());
        int id = rest1.getID().compareTo(rest2.getID());

        if (name == 0) {
            return id;
        } else {
            return name;
        }
    }

    public void passHashMap(MyHashMap<Long, Restaurant> hm) {
        restHM = hm; 
    }

}

class CompareRestaurantID implements Comparator<Restaurant> {
    @Override
    public int compare(Restaurant a, Restaurant b) {
        int date = a.getDateEstablished().compareTo(b.getDateEstablished()); 
        int name = a.getName().compareTo(b.getName()); 
        int id = a.getID().compareTo(b.getID()); 

        if (date == 0) {
            if (name == 0) {
                return id; 
            } else {
                return name; 
            }
        }
        return date; 
    }   
}

class CompareRestaurantDateNameIDlong implements Comparator<Long> {
    MyHashMap<Long, Restaurant> restHM; 

    @Override
    public int compare(Long id1, Long id2) {

        Restaurant rest1 = restHM.get(id1);
        Restaurant rest2 = restHM.get(id2);

        int date = rest1.getDateEstablished().compareTo(rest2.getDateEstablished());
        int name = rest1.getName().compareTo(rest2.getName()); 
        int id = rest1.getID().compareTo(rest2.getID()); 

        if (date == 0) {
            if (name == 0) {
                return id;
            } else {
                return name; 
            }
        }
        return date; 
    }   
    public void passHashMap(MyHashMap<Long, Restaurant> hm) {
        restHM = hm; 
    }

}

class CompareRestaurantDateNameIDrest implements Comparator<Restaurant> {
    MyHashMap<Long, Restaurant> restHM; 

    @Override
    public int compare(Restaurant a, Restaurant b) {

        int date = a.getDateEstablished().compareTo(b.getDateEstablished());
        int name = a.getName().compareTo(b.getName()); 
        int id = a.getID().compareTo(b.getID()); 

        if (date == 0) {
            if (name == 0) {
                return id;
            } else {
                return name; 
            }
        }
        return date; 
    }   
    public void passHashMap(MyHashMap<Long, Restaurant> hm) {
        restHM = hm; 
    }

}

class CompareRestaurantStars implements Comparator<Restaurant> {
    @Override
    public int compare(Restaurant a, Restaurant b) {
        int name = a.getName().compareTo(b.getName()); 
        int id = a.getID().compareTo(b.getID()); 
        int stars = Integer.compare(a.getWarwickStars(), b.getWarwickStars()); 
        stars *= -1;

        if (stars == 0) {
            if (name == 0) {
                return id; 
            } else {
                return name; 
            }
        }
        return stars; 

    }   
}

class CompareRating implements Comparator<Restaurant> {
    @Override
    public int compare(Restaurant a, Restaurant b) {
        int rating = Float.compare(a.getCustomerRating(), b.getCustomerRating()); 
        int name = a.getName().compareTo(b.getName()); 
        int id = a.getID().compareTo(b.getID()); 
        rating *= -1;

        if (rating == 0) {
            if (name == 0) {
                return id; 
            } else {
                return name;
            }
        }
        return rating; 
    }   
}

class CompareLongs implements Comparator<Long> {
    @Override
    public int compare(Long a, Long b) {
        return Long.compare(a, b); 
    }   
}