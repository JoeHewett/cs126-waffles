package uk.ac.warwick.cs126.stores;

import uk.ac.warwick.cs126.interfaces.ICustomerStore;
import uk.ac.warwick.cs126.models.Customer;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.io.IOUtils;

import uk.ac.warwick.cs126.structures.MyArrayList;
import uk.ac.warwick.cs126.structures.MyBinaryTree;
import uk.ac.warwick.cs126.structures.QuickSorter;
import uk.ac.warwick.cs126.structures.MyHashMap;
import uk.ac.warwick.cs126.structures.MyHashSet;


import uk.ac.warwick.cs126.util.DataChecker;
import uk.ac.warwick.cs126.util.StringFormatter;


import java.util.Comparator;

public class CustomerStore implements ICustomerStore {

    private DataChecker dataChecker;

    private MyHashSet<Long> blacklist; 
    private MyBinaryTree<Long> customerIDBinaryTree; 
    private MyBinaryTree<Long> customerBinaryTree;
    private MyHashMap<Long, Customer> customerHashMap; 

    private CompareCustomer customerComparator; 

    public CustomerStore() {

        customerComparator = new CompareCustomer(); 

        dataChecker = new DataChecker();
        
        blacklist = new MyHashSet<Long>();    
        
        customerBinaryTree = new MyBinaryTree<Long>(customerComparator);
        customerIDBinaryTree = new MyBinaryTree<Long>(new CompareLong());    

        customerHashMap = new MyHashMap<>(); 

    }

    public Customer[] loadCustomerDataToArray(InputStream resource) {
        Customer[] customerArray = new Customer[0];

        try {
            byte[] inputStreamBytes = IOUtils.toByteArray(resource);
            BufferedReader lineReader = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(inputStreamBytes), StandardCharsets.UTF_8));

            int lineCount = 0;
            String line;
            while ((line=lineReader.readLine()) != null) {
                if (!("".equals(line))) {
                    lineCount++;
                }
            }
            lineReader.close();

            Customer[] loadedCustomers = new Customer[lineCount - 1];

            BufferedReader csvReader = new BufferedReader(new InputStreamReader(
                    new ByteArrayInputStream(inputStreamBytes), StandardCharsets.UTF_8));

            int customerCount = 0;
            String row;
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

            csvReader.readLine();
            while ((row = csvReader.readLine()) != null) {
                if (!("".equals(row))) {
                    String[] data = row.split(",");

                    Customer customer = (new Customer(
                            Long.parseLong(data[0]),
                            data[1],
                            data[2],
                            formatter.parse(data[3]),
                            Float.parseFloat(data[4]),
                            Float.parseFloat(data[5])));

                    loadedCustomers[customerCount++] = customer;
                }
            }
            csvReader.close();

            customerArray = loadedCustomers;

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }

        return customerArray;
    }

    /**
    * add a customer to customerArray list
    * @return true if success, false if attempt was invalid
    * @param customer the customer object to add to the list
    */
    public boolean addCustomer(Customer customer) {
        
        if (!(dataChecker.isValid(customer))) {
            return false;
        }
        
        Long id = customer.getID(); 

        if (!(blacklist.contains(id))) {
            if (customerHashMap.get(id) == null) {
                customerHashMap.put(id, customer);
                customerComparator.passHashMap(customerHashMap); 

                customerIDBinaryTree.add(id);
                customerBinaryTree.add(customer.getID());
                return true; 
            } else {
                customerHashMap.remove(id);
                blacklist.add(id); 
                customerBinaryTree.removeFromTree(customer.getID()); 
                customerIDBinaryTree.removeFromTree(id); 
                return false; 
            }
        }
        
        return false;

    }

    /**
    * add an array of customer to customerArray list
    * @return true if success, false if any attempted customer add was invalid
    * @param customers the array of customers to add to the list
    */
    public boolean addCustomer(Customer[] customers) {
        boolean success = true; 
        for (int i = 0; i < customers.length; i++) {
            if (!addCustomer(customers[i])) {
                success = false; 
            }
        } 
        return success; 
    } 

    /**
    * Retrieve the customer object from the arrayList of customers using the ID 
    * @return customer object if found, null otherwise
    * @param id the id of the customer object being searched for
    */
    public Customer getCustomer(Long id) {
        return customerHashMap.get(id);
    }

    /**
    * Retrieve an array containing all of the customers in the customerArray list 
    * @return a sorted array of customer objects ordered by ID  
    */
    public Customer[] getCustomers() {
        MyArrayList<Long> custIDs = new MyArrayList<>();
        custIDs = customerIDBinaryTree.inOrderTraversal();

        Customer[] custsInOrderArray = new Customer[custIDs.size()];

        for (int i = 0; i < custsInOrderArray.length; i++) {
            custsInOrderArray[i] = customerHashMap.get(custIDs.get(i));
        }

        return custsInOrderArray;
    }

    public Customer[] getCustomers(Customer[] customers) {
        QuickSorter<Customer> qs = new QuickSorter<>();
        return qs.quickSort(customers, 0, customers.length - 1, new CompareCustomerID()); 

    }

    public Customer[] getCustomersByName() {
        MyArrayList<Long> custsInOrder = new MyArrayList<>();
        custsInOrder = customerBinaryTree.inOrderTraversal();

        Customer[] custsInOrderArray = new Customer[custsInOrder.size()];

        for (int i = 0; i < custsInOrderArray.length; i++) {
            custsInOrderArray[i] = getCustomer(custsInOrder.get(i));
        }

        return custsInOrderArray;
    }

    public Customer[] getCustomersByName(Customer[] customers) {
        QuickSorter<Customer> qs = new QuickSorter<>();
        return qs.quickSort(customers, 0, customers.length - 1, new CompareCustomerCust()); 

    }

    public Customer[] getCustomersContaining(String searchTerm) {
        if (searchTerm == null) {
            return new Customer[0];
        }                
        String searchTermConverted = StringFormatter.convertAccentsFaster(searchTerm).toLowerCase();

        Customer[] allCustomers = getCustomersByName();
        MyArrayList<Customer> matches = new MyArrayList<>();

        for (int i = 0; i < allCustomers.length; i++) {
            String fullName = allCustomers[i].getFirstName() + " " + allCustomers[i].getLastName();
            fullName = fullName.toLowerCase();

            if (fullName.contains(searchTermConverted)) {
                matches.add(allCustomers[i]);
            }
        }

        Customer[] matchesArray = new Customer[matches.size()];
        for (int x = 0; x < matches.size(); x++) {
            matchesArray[x] = matches.get(x);

        }

        return matchesArray;
    }

}

class CompareCustomer implements Comparator<Long> {
    MyHashMap<Long, Customer> custHM; 

    @Override
    public int compare(Long id1, Long id2) {
        Customer cust1 = custHM.get(id1);
        Customer cust2 = custHM.get(id2); 

        int lastName = cust1.getLastName().toLowerCase().compareTo(cust2.getLastName().toLowerCase());
        int firstName = cust1.getFirstName().toLowerCase().compareTo(cust2.getFirstName().toLowerCase());
        int id = cust1.getID().compareTo(cust2.getID()); 
    
        if (lastName == 0) {
            if (firstName == 0) {
                return id;
            } else {
                return firstName;
            }
        }
        return lastName;     
    } 

    public void passHashMap(MyHashMap<Long, Customer> hm) {
        custHM = hm; 
    }
}

class CompareCustomerCust implements Comparator<Customer> {

    @Override
    public int compare(Customer a, Customer b) {

        int lastName = a.getLastName().toLowerCase().compareTo(b.getLastName().toLowerCase());
        int firstName = a.getFirstName().toLowerCase().compareTo(b.getFirstName().toLowerCase());
        int id = a.getID().compareTo(b.getID()); 
    
        if (lastName == 0) {
            if (firstName == 0) {
                return id;
            } else {
                return firstName;
            }
        }
        return lastName;     
    } 

}

class CompareCustomerID implements Comparator<Customer> {
    @Override
    public int compare(Customer a, Customer b) {
        return a.getID().compareTo(b.getID()); 
    }   
}

class CompareLong implements Comparator<Long> {
    @Override
    public int compare(Long a, Long b) {
        return Long.compare(a, b); 
    }   
}


